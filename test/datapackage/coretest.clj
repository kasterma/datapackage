(ns datapackage.coretest
    (:use datapackage.core
          midje.sweet))

(fact  "test reading of meta files"
  (read-meta "testdata/incorrect1") => (throws Exception "required fields missing")
  (read-meta "testdata/incorrect2") => (throws Exception "required fields missing")
  (read-meta "testdata/country-codes") =>
  {"name" "country-codes",
   "resources" [{"path" "country-codes.csv", "schema" {"fields" [{"id" "Code"} {"id" "Country"}]}}],
   "sources" [{"name" "ISO", "web" "http://www.iso.org/iso/home/standards/country_codes.htm"}],
   "title" "ISO 2 digit country codes"})
