(ns datapackage.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.logic :as logic]
            [clojure.algo.monads :as monads]
            [instaparse.core :as insta]
            [clojure.core.typed :as typed]
            [cliopatra.command :as command :refer [defcommand]]
            [clojure.pprint :as pprint])
  (:use midje.sweet)
  (:gen-class))

(timbre/set-level! :trace)

(defn has-required-datapackagejson-fields
  "check the required field are present in the datapackage.json map
   representation"
  [m]
  (let [name       (m "name")
        resources  (m "resources")]
    (and
     (not (nil? name))
     (reduce (fn [t res]
               (and t (or (contains? res "path")
                          (contains? res "url"))))
             true  resources))))

(defn read-meta
  "read and parse the datapackage.json file"
  [dir]
  (let [m (json/read-str (slurp (str dir "/" "datapackage.json")))]
    (if (has-required-datapackagejson-fields m)
      m
      (do (timbre/error "required fields were not present in datapackage.json:" dir)
          (throw ( Exception. "required fields missing"))))))

(defcommand test1
  "basic command to run some tests on the first datapackage example"
  {:opts-spec []}
  (timbre/info "running test1")
  (pprint/pprint (read-meta "testdata/country-codes")))

(defn -main [& args]
  (command/dispatch 'datapackage.core args))
